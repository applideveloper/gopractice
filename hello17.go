// range

package main

import "fmt"

func main() {
//    s := []int{2, 3, 8}
//    for i, v := range s {
//        fmt.Println(i, v)
//    }

    // _ : ブランク修飾子、何が入ってきてもそれを廃棄してくれる
//    for _, v := range s {
//        fmt.Println(v)
//    }

    m := map[string]int{"taguchi": 200, "fkoji": 300}
    for k, v := range m {
        fmt.Println(k, v)
    }
}
