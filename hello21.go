// インターフェース
// メソッド
// 構造体
// インターフェースは、メソッドの一覧を定義したデータ型なのですが、
// それらのメソッドをある構造体が実装していれば、
// その構造体をインターフェース型として扱っても良い

// インターフェース型には、空のインターフェース型というものも定義することができます。
// interface{}
// この場合、実装すべきメソッドがないので、実際的には
// 「全てのデータ型がこのインターフェースを満たしている」と言えます。

package main

import "fmt"

// 空のインターフェース型を引数をとると
// あらゆるデータ型をここで受け取ることができます。
// 処理の中においては、そのデータ型が何かをしる必要があります。
// 空のインターフェース型が何かをしる手法は2つあって、
// 一つは型アサーションというとのと、もう一つは型スイッチというものがあります。
func show(t interface{}) {
    // 型アサーション
    // 第一返り値: 値自体、第二返り値: japaneseかどうかの真偽値
//    _, ok := t.(japanese)
//    if ok {
//        fmt.Println("I am Japanese")
//    } else {
//        fmt.Println("I am not Japanese")
//    }
    
    // 型Switch
    // 渡ってきたデータの名前が返ってきます
    switch t.(type) {
        case japanese:
            fmt.Println("I am Japanese")
        default:
            fmt.Println("I am not Japanese")
    }
}

// インターフェース
type greeter interface {
    greet()
}

type japanese struct {}
type american struct {}
// japaneseもamericanも同じインターフェースを満たしていれば、
// その型でもって両方の型をスライスに入れることができるようになるので、そういったことをやる

func (j japanese) greet() {
    fmt.Println("こんにちは")
}

func (a american) greet() {
    fmt.Println("Hello!")
}

func main() {
    // 同じインターフェースの型を満たしているので、スライスに入れることができる
    greeters := []greeter{japanese{}, american{}}
    for _, greeter := range greeters {
        greeter.greet()
        show(greeter)
    }
}
