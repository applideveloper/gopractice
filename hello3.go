// 変数
// 変数名: 1文字目に注意
// 1文字目が小文字ならそのパッケージの中だけで見える変数になる
// 1文字目が大文字の場合、他のパッケージからも見えてしまう変数になる
// これは、変数も定数も後から出てくる関数も全部同様で、よくみると
// fmtパッケージの中のPrintlnの1文字目が大文字になって、
// 他のパッケージから使えるようになっている
package main

import "fmt"

func main() {
    // var msg string
    // msg = "hello world"

    // var msg = "hello world"
    
    msg := "hello world"
    fmt.Println(msg)

    // var a int, b int
    //var a, b int
    // a, b = 10, 15
    a, b := 10, 15

    var (
        c int 
        d string
    )
    c = 20
    d = "hoge"
    
}
