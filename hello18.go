// 構造体

package main

import "fmt"

type user struct {
    name string
    score int
}

func main() {
    // userの構造体分の領域を確保して、それぞれ0とか空文字で初期化して、
    // そのポインタを返してくれるというものになる
//    u := new(user)
//    // (*u).name = "taguchi"
//    u.name = "taguchi"
//    u.score = 20

//    u := user{"taguchi", 50}
    u := user{name:"taguchi", score:50 }
    fmt.Println(u)
}
