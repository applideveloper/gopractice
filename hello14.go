// if
/*
> >=
< <=
==
!=
&&
||
!
*/

package main

import "fmt"

func main() {
//    score := 43
//    
//    if score > 80 {
//        fmt.Println("score > 80")
//    } else if score > 60 {
//        fmt.Println("60 < score <= 80")
//    } else {
//        fmt.Println("60 <= score")
//    }

    if score := 43; score > 80 {
        // scoreのscopeはifのscopeの中だけ
        fmt.Println("score > 80")
    } else if score > 60 {
        fmt.Println("60 < score <= 80")
    } else {
        fmt.Println("60 <= score")
    }
}
