// 基本データ型
/*
string  "hello"
int     53
int8 int16 int32 int64
float32 float64
float64     10.2
bool        false true
nil

var s string    // ""
var a int        // 0
var f bool      // false
*/

package main

import "fmt"

func main() {
    a := 10
    b := 12.3
    c := "hoge"
    var d bool
    var e int
    fmt.Printf("a: %d, b:%f, c:%s, d:%t, e:%v\n", a, b, c, d, e)
}
