// インターフェース
// メソッド
// 構造体
// インターフェースは、メソッドの一覧を定義したデータ型なのですが、
// それらのメソッドをある構造体が実装していれば、
// その構造体をインターフェース型として扱っても良い

package main

import "fmt"

// インターフェース
type greeter interface {
    greet()
}

type japanese struct {}
type american struct {}
// japaneseもamericanも同じインターフェースを満たしていれば、
// その型でもって両方の型をスライスに入れることができるようになるので、そういったことをやる

func (j japanese) greet() {
    fmt.Println("こんにちは")
}

func (a american) greet() {
    fmt.Println("Hello!")
}

func main() {
    // 同じインターフェースの型を満たしているので、スライスに入れることができる
    greeters := []greeter{japanese{}, american{}}
    for _, greeter := range greeters {
        greeter.greet()
    }
}
