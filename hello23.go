// goroutine : 平行処理
// channel
// データを受け渡すパイプ

package main

import (
    "fmt"
    "time"
)

func task1(result chan string) {
    time.Sleep(time.Second * 2)
    // fmt.Println("task1 finished!")
    result <- "task1 result"
}

func task2() {
    fmt.Println("task2 finished!")
}

func main() {
    // chanel
    // スライスと同じ参照型
    result := make(chan string)
    go task1(result) // バックグラウンドで実行
    go task2() // バックグラウンドで実行 // resultに入ってくるのを待つ間に実行される

    // 一番最初に実行される
    // resultの中に何も入ってない場合は、入ってくるまで待つ
    fmt.Println(<-result)

    // goroutineが終わる前にmain関数が終わってしまうので、
    time.Sleep(time.Second * 3)
}
