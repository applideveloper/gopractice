// 構造体
// メソッド

package main

import "fmt"

type user struct {
    name string
    score int
}

// レシーバー(u user) は値渡し
func (u user)show () {
// 参照渡しは(u *user)
// func (u *user)show () {
    fmt.Printf("name:%s, score:%d\n", u.name, u.score)
}

// 値渡し
func (u user) nohit() {
    u.score++
}

// 参照渡し
func (u *user) hit() {
    u.score++
}

func main() {
    u := user{name:"taguchi", score:50}
    // fmt.Println(u)
    u.show()
    u.nohit()
    u.show()
    u.hit()
    u.show()
}
