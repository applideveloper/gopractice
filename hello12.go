// スライス

package main

import "fmt"

func main() {
    // いきなりスライスを作る
    // s := make([]int, 3) // [0, 0, 0]
    s := []int{1, 3, 5}

    // append
    s = append(s, 8, 2, 10)
    // copy 
    t := make([]int, len(s))
    n := copy(t, s) // コピーした要素の数を数える
    fmt.Println(s)
    fmt.Println(t)
    fmt.Println(n)
}
