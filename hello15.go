// switch

package main

import "fmt"

func main() {
//    signal := "blue"
//    switch signal {
//        case "red":
//            fmt.Println("red")
//        case "yello":
//            fmt.Println("yellow")
//        case "green", "blue":
//            fmt.Println("green or blue")
//        default:
//            fmt.Println("default")
//    }

    score := 80
    switch {
        case score > 80:
            fmt.Println("score > 80")
        case score > 60:
            fmt.Println("60 < score <= 80")
        default:
            fmt.Println("default")
    }
}
